//
//  FinManager.swift
//  FinanceApp
//
//  Created by iMac5K on 24.03.16.
//  Copyright © 2016 theUberSoft. All rights reserved.
//

import Foundation

let notificationUpdated = "indexesUpdated"

class FinManager {
    class var sharedInstance: FinManager {
        struct Static {
            static let instance: FinManager = FinManager()
        }
        
        return Static.instance
    }
    
    func updateList(indexes: [Index]) -> () {
        //http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.quotes where symbol IN ("AAPL","GOOG")&format=json&env=http://datatables.org/alltables.env
        
        if indexes.count == 0 {
            return
        }
        
        var indexesStr = "("
        
        for i in 0..<(indexes.count - 1) {
            indexesStr += "\"\(indexes[i].indexName)\","
        }
        
        indexesStr += "\"\(indexes.last!.indexName)\")"
        
        let urlStr: String = ("http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.quotes where symbol IN \(indexesStr)&format=json&env=http://datatables.org/alltables.env").stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        let url: NSURL = NSURL(string: urlStr)!
        let request: NSURLRequest = NSURLRequest(URL:url)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        let task: NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if ((error) != nil) {
                print(error!.localizedDescription)
            }
            else {
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
                    let results = ((json!.objectForKey("query") as! NSDictionary).objectForKey("results") as! NSDictionary)
                    var quotes: [NSDictionary] = []
                    
                    if let q = results.objectForKey("quote") as? [NSDictionary] {
                        quotes = q
                    }
                    else if let q = results.objectForKey("quote") as? NSDictionary {
                        quotes += [q]
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        NSNotificationCenter.defaultCenter().postNotificationName(notificationUpdated, object: nil, userInfo: [notificationUpdated:quotes])
                    })
                }
                catch {
                    print("JSON Error: \(error)")
                }
                
            }
            
        })
        
        task.resume()
    }
}