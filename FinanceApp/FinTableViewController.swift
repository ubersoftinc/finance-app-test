//
//  ViewController.swift
//  FinanceApp
//
//  Created by iMac5K on 24.03.16.
//  Copyright © 2016 theUberSoft. All rights reserved.
//

import UIKit

class FinCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var changePercentLabel: UILabel!
    @IBOutlet weak var currentPriceLabel: UILabel!
    @IBOutlet weak var closePriceLabel: UILabel!
    @IBOutlet weak var daysLowLabel: UILabel!
    @IBOutlet weak var daysHidhLabel: UILabel!
}

struct Index {
    let indexName: String
    var changePercent: String = ""
    var currentPrice: String = ""
    var closePrice: String = ""
    var daysLow: String = ""
    var daysHidh: String = ""
    
    init(indexName: String) {
        self.indexName = indexName
    }
}

class FinTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var indexes: [Index] = []
    private let updateTime = 120.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // customize tableView
        tableView.contentInset = UIEdgeInsetsMake(100, 0, 0, 0)
        tableView.separatorInset = UIEdgeInsetsZero
        tableView.tableFooterView = UIView()
        
        loadUserIndexes()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FinTableViewController.indexesUpdated(_:)), name: notificationUpdated, object: nil)
        self.updateList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: load
    
    func loadUserIndexes() {

//        let defaults = NSUserDefaults.standardUserDefaults()
//        defaults.setObject("Coding Explorer", forKey: "userNameKey")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        var initialIndexes = defaults.stringArrayForKey("initialIndexes")
        
        if initialIndexes == nil {
            initialIndexes = ["AAPL", "GOOG", "INTC",  "TWTR", "FB", "NFLX", "AMZN", "LNKD", "YHOO"]
            defaults.setObject(initialIndexes, forKey: "initialIndexes")
        }

        for index in initialIndexes! {
            indexes.append(Index.init(indexName: index))
        }
    }
    
    func saveNewIndex(newIndex: String) {
        let defaults = NSUserDefaults.standardUserDefaults()
        var initialIndexes = defaults.stringArrayForKey("initialIndexes")
        
        if initialIndexes == nil {
            initialIndexes = []
        }
        
        initialIndexes?.append(newIndex)
        defaults.setObject(initialIndexes, forKey: "initialIndexes")
    }
    
    func deleteSavedIndex(deleteIndex: String) {
        let defaults = NSUserDefaults.standardUserDefaults()
        var initialIndexes = defaults.stringArrayForKey("initialIndexes")
        
        if initialIndexes == nil {
            initialIndexes = []
        }
        
        for i in 0..<initialIndexes!.count {
            if initialIndexes![i] == deleteIndex {
                initialIndexes!.removeAtIndex(i)
                break
            }
        }

        defaults.setObject(initialIndexes, forKey: "initialIndexes")
    }
    
    // MARK: update
    
    func updateList() {
        let finManager: FinManager = FinManager.sharedInstance
        
        finManager.updateList(indexes)
        
        // repeat after 15 secs
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(updateTime * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
            self.updateList()
        })
    }
    
    func indexesUpdated(notification: NSNotification) {
        let values = (notification.userInfo as! Dictionary<String, NSArray>)
        let indexesReceived:NSArray = values[notificationUpdated]!
        
        for quote in indexesReceived {
            let quoteDict:NSDictionary = quote as! NSDictionary
            
            for i in 0..<indexes.count {
                if indexes[i].indexName == quoteDict["symbol"] as! String {
                    if let currentPrice = quoteDict["Ask"] as? String {
                        indexes[i].currentPrice  = currentPrice
                        indexes[i].changePercent = quoteDict["ChangeinPercent"] as! String
                        indexes[i].closePrice    = quoteDict["PreviousClose"]   as! String
                        indexes[i].daysLow       = quoteDict["DaysLow"]         as! String
                        indexes[i].daysHidh      = quoteDict["DaysHigh"]        as! String
                    }
                    break
                }
            }
        }
        
        tableView.reloadData()
        //print("updated")
    }
    
    // MARK: table view

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FinCell", forIndexPath: indexPath) as! FinCell

        var percentStr: String = indexes[indexPath.row].changePercent
        let cellBackgroundAlpha: CGFloat = 0.7
        let defColor: UIColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: cellBackgroundAlpha)
        
        if percentStr.characters.count > 0 {
            percentStr = percentStr.substringToIndex(percentStr.endIndex.predecessor())
            
            if let percentValue = Double(percentStr) {
                switch percentValue {
                case let x where x < 0.0:
                    cell.backgroundColor = UIColor(red: 255/255, green: 109/255, blue: 80/255, alpha: cellBackgroundAlpha)
                case let x where x > 0.0:
                    cell.backgroundColor = UIColor(red: 83/255, green: 208/255, blue: 145/255, alpha: cellBackgroundAlpha)
                case _:
                    cell.backgroundColor = defColor
                }
                
                if percentValue < -1.5 || percentValue > 1.5 {
                    cell.changePercentLabel.font = UIFont(name:"HelveticaNeue-Medium", size: 11.0)
                }
                else {
                    cell.changePercentLabel.font = UIFont(name:"HelveticaNeue-Thin", size: 11.0)
                }
            }
        }
        else {
            cell.backgroundColor = defColor
        }
        
        
        
        cell.layoutMargins = UIEdgeInsetsZero
 
        cell.nameLabel.text          = indexes[indexPath.row].indexName
        cell.changePercentLabel.text = indexes[indexPath.row].changePercent
        cell.currentPriceLabel.text  = indexes[indexPath.row].currentPrice
        cell.closePriceLabel.text    = indexes[indexPath.row].closePrice
        cell.daysLowLabel.text       = indexes[indexPath.row].daysLow
        cell.daysHidhLabel.text      = indexes[indexPath.row].daysHidh
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        searchBar.endEditing(true)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, willBeginEditingRowAtIndexPath indexPath: NSIndexPath) {
        searchBar.endEditing(true)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            deleteSavedIndex(indexes[indexPath.row].indexName)
            indexes.removeAtIndex(indexPath.row)
            tableView.reloadData()
        }
    }
    
    // MARK: search bar
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        tableView.setEditing(false, animated: true)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRowAtIndexPath(indexPath, animated: false)
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if indexes.indexOf({ $0.indexName == searchBar.text }) == nil {
            //indexes.insert(Index.init(indexName: searchBar.text!), atIndex: 0)
            indexes.append(Index.init(indexName: searchBar.text!))
            tableView.reloadData()
            
            saveNewIndex(searchBar.text!)
            
            searchBar.text = ""
            
            let finManager: FinManager = FinManager.sharedInstance
            finManager.updateList(indexes)
        }
    }
}

